CREATE TABLE IF NOT EXISTS PRODUCTOS (id BIGINT PRIMARY KEY AUTO_INCREMENT,
                        nombre VARCHAR(255) NOT NULL,
                        precio DOUBLE NOT NULL,
                        precio_descuento DOUBLE,
                        porcentaje_descuento DOUBLE);

CREATE TABLE IF NOT EXISTS IMAGENES (url VARCHAR(4000) NOT NULL,
                      tipo VARCHAR(1) NOT NULL,
                      producto_id BIGINT NOT NULL,
                      CONSTRAINT fk_imagenes_prod_id FOREIGN KEY (producto_id) REFERENCES PRODUCTOS(id));

CREATE TABLE IF NOT EXISTS DESCUENTOS (porcentaje DOUBLE NOT NULL,
                         fecha_inicio DATE NOT NULL,
                         fecha_fin DATE NOT NULL,
                         producto_id BIGINT NOT NULL,
                         CONSTRAINT fk_descuentos_prod_id FOREIGN KEY (producto_id) REFERENCES PRODUCTOS(id));
