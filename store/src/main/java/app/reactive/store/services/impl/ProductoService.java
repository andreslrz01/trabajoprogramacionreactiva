package app.reactive.store.services.impl;

import app.reactive.store.model.dto.ProductoDTO;
import app.reactive.store.repositories.ProductoRepository;
import app.reactive.store.services.IProductoService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class ProductoService implements IProductoService {

	private final ProductoRepository productoRepository;

	@Override
	public Flux<ProductoDTO> getAll() {
		return productoRepository.findAll().map(p -> new ProductoDTO(p.getNombre()));
	}

	@Override
	public Mono<ProductoDTO> getById(long id) {
		return null;
	}

	@Override
	public Mono<ProductoDTO> create(ProductoDTO producto) {
		return null;
	}

	@Override
	public Mono<ProductoDTO> update(ProductoDTO producto) {
		return null;
	}

	@Override
	public Mono<ProductoDTO> delete(long id) {
		return null;
	}
}
