package app.reactive.store.services;

import org.springframework.stereotype.Service;


import app.reactive.store.model.dto.ProductoDTO;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public interface IProductoService {
	Flux<ProductoDTO> getAll();
	Mono<ProductoDTO> getById(long id);
	Mono<ProductoDTO> create(ProductoDTO producto);
	Mono<ProductoDTO> update(ProductoDTO producto);
	Mono<ProductoDTO> delete(long id);
}
