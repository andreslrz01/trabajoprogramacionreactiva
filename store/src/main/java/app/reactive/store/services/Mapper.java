package app.reactive.store.services;

@FunctionalInterface
public interface Mapper<S, T> {
	public T map(S source);
}
