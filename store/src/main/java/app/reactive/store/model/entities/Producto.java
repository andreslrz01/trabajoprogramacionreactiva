package app.reactive.store.model.entities;

import lombok.*;
import org.springframework.data.annotation.Id;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class Producto {
	@Id
	private Long id;
	private String nombre;
	private Double precio;
	private Double precioConDescuento;
	private Double porcentajeDescuento;
	private List<Imagen> imagenes;
	private List<Descuento> descuentos;

	public Producto(String nombre, Double precio){
		this.nombre = nombre;
		this.precio = precio;
	}
}
