package app.reactive.store.model.entities;

import lombok.*;

import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Descuento {
	private Double porcentaje;
	private LocalDate fechaInicio;
	private LocalDate fechaFin;
}
