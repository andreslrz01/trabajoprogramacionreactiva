package app.reactive.store.model.entities;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Imagen {
	private String url;
	private boolean frontal;
	private boolean trasera;
}
