package app.reactive.store.vendors;

import app.reactive.store.model.dto.ProductoDTO;
import app.reactive.store.services.IProductoService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.validation.Validator;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import static java.lang.Long.parseLong;

@Component
@RequiredArgsConstructor
public class ProductoHandler {
    private final IProductoService service;
    private final Validator validator;

    public Mono<ServerResponse> getAll(ServerRequest request){
        return ServerResponse.ok()
                             .contentType(MediaType.APPLICATION_JSON)
                             .body(service.getAll(), ProductoDTO.class);
    }

    public Mono<ServerResponse> getById(ServerRequest request){
        return ServerResponse.ok()
                             .contentType(MediaType.APPLICATION_JSON)
                             .body(service.getById(parseLong(request.pathVariable("id"))), ProductoDTO.class);
    }
}
