package app.reactive.store;

import app.reactive.store.model.entities.Producto;
import app.reactive.store.repositories.ProductoRepository;
import io.r2dbc.spi.ConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.r2dbc.connection.init.ConnectionFactoryInitializer;
import org.springframework.r2dbc.connection.init.ResourceDatabasePopulator;

import java.util.Arrays;

@SpringBootApplication
public class StoreApplication {
	private static final Logger log = LoggerFactory.getLogger(StoreApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(StoreApplication.class, args);
	}

	@Bean
	ConnectionFactoryInitializer initializer(ConnectionFactory connectionFactory) {

		ConnectionFactoryInitializer initializer = new ConnectionFactoryInitializer();
		initializer.setConnectionFactory(connectionFactory);
		initializer.setDatabasePopulator(new ResourceDatabasePopulator(new ClassPathResource("schema.sql")));

		return initializer;
	}

	@Bean
	public CommandLineRunner demo(ProductoRepository productoRepository) {
		return (args) -> {
			productoRepository.saveAll(Arrays.asList(new Producto("Zapatos Adidas", 14000.00),
					new Producto("Chloe", 17000.00),
					new Producto("Kim", 24000.00),
					new Producto("David", 90000.00),
					new Producto("Michelle", 78000.00)));

			log.info("registrado");
		};
	}
}
